package cc.trading;

import java.text.SimpleDateFormat;
import java.util.Date;

import cc.bean.Stock;
import cc.bean.Transaction;
import cc.enums.Action;
import cc.enums.BrokerResponse;
import cc.enums.StrategyStatus;

public class MACDStrategy extends Strategy{
    private int flag = 0;
    private int previousFlag;
    private Action action;
    
    private MovingAverage longTerm;
    private MovingAverage shortTerm;
    
    public MACDStrategy(Stock stock, int length_long, int length_short) {
        super(stock);
        this.longTerm = new MovingAverage(length_long);
        this.shortTerm = new MovingAverage(length_short);
        action = Action.NONE;
    }
    
    public void twoMAStrategy() {
        String buyOrSell;
        double currentPrice = stock.getPrice ();
        longTerm.addNumber(currentPrice);
        shortTerm.addNumber(currentPrice);
        
        double shortAvg = shortTerm.calculateAvg();
        double longAvg = longTerm.calculateAvg();
        
        if (shortAvg != 0 && longAvg != 0) {
            if (flag == 0) {
                flag = shortAvg > longAvg ? 1 : -1;
                previousFlag = flag;
            }
            else {
                flag =  shortAvg > longAvg ? 1 : -1;
                if (flag != previousFlag) {
                    action = flag > previousFlag ? Action.BUY : Action.SELL;
                    createTransaction();
                    String strategy = action + " at Price=" + currentPrice ;
                    System.out.println(strategy);
                } 
                previousFlag = flag;
            }
        }
    }
    
    public BrokerResponse createTransaction() {
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Transaction newTrans = new Transaction (format.format(now), stock.getPrice (), 1000, stock.getTicker(), this.action);
        // JPAService.addTransaction(newTrans);
        
        
        return BrokerResponse.ACCEPT;
    }
    
    public double getLongTermAvg() {
        return longTerm.calculateAvg();
    }
    
    public double getShortTermAvg() {
        return shortTerm.calculateAvg();
    }

}
