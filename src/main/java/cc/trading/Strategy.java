package cc.trading;

import cc.bean.Stock;
import cc.enums.Action;

public class Strategy
{
    protected Stock stock;
    protected Action action;
    
    public Strategy(Stock stock) {
        setStock(stock);
    }
    
    public void setStock(Stock stock) {
        this.stock = stock;
    }
    
    public Stock getStock() {
        return this.stock;
    }
    
    public Action getAction() {
        return this.action;
    }
    

}
