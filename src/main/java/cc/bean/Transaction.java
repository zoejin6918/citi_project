package cc.bean;

import java.util.Date;

import cc.enums.Action;
import cc.enums.Company;


public class Transaction
{
    private int trans_id;
    private String time; // new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private double price;
    private int amount;
    private Company ticker;
    private Action action;
    
    public Transaction() {
        
    }

    public Transaction (String time, double price, int amount, Company ticker,
        Action action)
    {
        super ();
        setTime(time);
        setPrice(price);
        setAmount(amount);
        setTicker(ticker);
        setAction(action);
        
    }
    
    public int getTransId() {
        return trans_id;
    }
    
    public void setTransId(int id) {
        this.trans_id = id;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public int getAmount ()
    {
        return amount;
    }

    public void setAmount (int amount)
    {
        this.amount = amount;
    }

    public Company getTicker ()
    {
        return ticker;
    }

    public void setTicker (Company ticker)
    {
        this.ticker = ticker;
    }

    public Action getAction ()
    {
        return action;
    }

    public void setAction (Action action)
    {
        this.action = action;
    }
    
    

}
