package cc.bean;

import cc.enums.Company;

public class Stock
{
    private Company ticker;
    private double price;
    private double avgCost;
    private double profitOrLose;
    
    public Stock() {
        
    }
    
    public Stock (Company ticker, double price, double avgCost,
        double profitOrLose)
    {
        super ();
        setTicker(ticker);
        setPrice(price);
        setAvgCost(avgCost);
        setProfitOrLose(profitOrLose);
    }

    public double updatePL() {
        return profitOrLose;
    }
    
    
    
    public Company getTicker ()
    {
        return ticker;
    }

    public void setTicker (Company ticker)
    {
        this.ticker = ticker;
    }

    public double getPrice ()
    {
        return price;
    }

    public void setPrice (double price)
    {
        this.price = price;
    }

    public double getAvgCost ()
    {
        return avgCost;
    }

    public void setAvgCost (double avgCost)
    {
        this.avgCost = avgCost;
    }

    public double getProfitOrLose ()
    {
        return profitOrLose;
    }

    public void setProfitOrLose (double profitOrLose)
    {
        this.profitOrLose = profitOrLose;
    }
}
