package cc.enums;

public enum StrategyStatus
{
    START,
    STOP,
    PAUSE,
    RESTART;
}
