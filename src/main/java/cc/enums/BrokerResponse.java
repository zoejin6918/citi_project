package cc.enums;

public enum BrokerResponse
{
    ACCEPT,
    DENIED,
    PARTIAL_ACCEPT;
}
